# Base

Project template build on Vue JS and Dotnet

## Prerequisites

* Git
* Docker

or

* Dotnet core 3.1
* Node v15
* Postgresql v11

### Start Docker Services 

Navigate to the project root where the docker-compose.yml file is located
```
docker-compose up -d
```

### Start Back End Server via Docker

Enter the dotnet container via an interactive shell
```
docker-compose exec back-end bash
```

Start the backend server
```
dotnet run -v BackEnd.API
```

### Start Front End Server via Docker

Enter the Node container via an interactive shell
```
docker-compose exec front-end bash
```

Start the node server
```
npm run serve
```

## Configure VS Code to debug dotnet dontainer

Add the following configuration to Launch.json
```
        {
            "name": ".NET Core Docker Attach",
            "type": "coreclr",
            "request": "attach",
            "processId": "${command:pickRemoteProcess}",
            "pipeTransport": {
                "pipeProgram": "docker",
                "pipeArgs": [ "exec", "-i", "back-end" ],
                "debuggerPath": "/remote_debugger/vsdbg",
                "pipeCwd": "${workspaceRoot}",
                "quoteArgs": false
            },
            "sourceFileMap": {
                "/back-end": "${workspaceRoot}"
            }
        }
```