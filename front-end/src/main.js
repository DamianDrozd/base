import Vue from 'vue'
import App from './App.vue'
import store from './store';
import router from './router';
import vuetify from './plugins/vuetify';
import axios from 'axios';
import vueAnime from 'vue-animejs';
import VueAnalytics from 'vue-analytics';

const isProd = process.env.NODE_ENV === 'production';

Vue.config.productionTip = false;

Vue.use(VueAnalytics, {
  id: process.env.VUE_APP_GOOGLE_ANALYTICS_TRACKING_ID,
  router,
  debug: {
    enabled: !isProd,
    sendHitTask: isProd
  }
})
Vue.use(vueAnime);

store.$axios = axios
.create({
    baseURL: process.env.VUE_APP_BLOG_API_URL
});
store.dispatch("checkSession");

new Vue({
  store,
  router,
  vuetify,
  render: h => h(App)
}).$mount('#app');