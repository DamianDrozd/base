import Vue from "vue";
import Vuetify from "vuetify/lib";
import GoogleIcon from "@/components/socials/GoogleIcon.vue";

Vue.use(Vuetify);

export default new Vuetify({
  icons: {
    values: {
      googleIcon: {
        component: GoogleIcon,
      },
    },
  },
});
