class account {
  constructor() {
    this.email = "";
    this.userName = "";
    this.password = "";
    this.confirmPassword = "";
    this.firstName = "";
    this.lastName = "";
    this.phoneNumber = "";
    this.token = "";
    this.authenticated = false;
    this.roles = [];
  }
}

const state = {
  account: new account(),
};

const getters = {
  account: (state) => state.account,
  username: (state) => state.account.username,
  roles: (state) => state.account.roles,
  loggedIn(state) {
    return !!state.account.authenticated;
  },
};

const actions = {
  async register({ commit, dispatch }) {
    return await this.$axios
      .post("/account/register", state.account)
      .then(function(response) {
        commit("SET_ACCOUNT", response.data);
      })
      .catch(function(error) {
        return HandleError(error, dispatch);
      });
  },

  async registerExternal({ commit, dispatch }, tokenModel) {
    return await this.$axios
      .post("/account/register-external", tokenModel)
      .then(function(response) {
        commit("SET_ACCOUNT", response.data);
      })
      .catch(function(error) {
        return HandleError(error, dispatch);
      });
  },

  async PasswordResetSend({ dispatch }, id) {
    return await this.$axios
      .post("/account/Password/reset/send", { Id: id })
      .then(function() {
        dispatch("addNotification", {
          msg:
            "If the account exists, a password reset will be sent to the registered email.",
          type: "success",
        });
      })
      .catch(function(error) {
        return HandleError(error, dispatch);
      });
  },

  async PasswordReset({ dispatch }, data) {
    return await this.$axios
      .post("/account/Password/reset", {
        UserName: data.UserName,
        Token: data.Token,
        Password: state.account.password,
        ConfirmPassword: state.account.confirmPassword,
      })
      .then(function() {
        dispatch("addNotification", {
          msg: "Successfully updated password.",
          type: "success",
        });
      })
      .catch(function(error) {
        return HandleError(error, dispatch);
      })
      .finally(() => {
        state.account.password = "";
        state.account.confirmPassword = "";
      });
  },

  async ConfirmEmail({ dispatch }, token) {
    return await this.$axios
      .post("/account/email/confirm", { Token: token })
      .then(function() {
        dispatch("addNotification", {
          msg: "Successfully confirmed your email.",
          type: "success",
        });
      })
      .catch(function(error) {
        return HandleError(error, dispatch);
      });
  },

  async SendConfirmationEmail({dispatch}){
    return await this.$axios
      .get("/account/email/confirm/send")
      .then(function() {
        dispatch("addNotification", {
          msg: "Sent out a confirmation email.",
          type: "success",
        });
      })
      .catch(function(error) {
        return HandleError(error, dispatch);
      });
  },

  async login({ commit, dispatch }) {
    return await this.$axios
      .post("/account/login", state.account)
      .then(function(response) {
        commit("SET_ACCOUNT", response.data);
      })
      .catch(function(error) {
        return HandleError(error, dispatch);
      })
      .finally(() => {
        state.account.password = "";
        state.account.confirmPassword = "";
      });
  },

  async loginExternal({ commit, dispatch }, _account) {
    return await this.$axios
      .post("/account/login-external", _account)
      .then(function(response) {
        commit("SET_ACCOUNT", response.data);
      })
      .catch(function(error) {
        return HandleError(error, dispatch);
      })
      .finally(() => {
        state.account.password = "";
        state.account.confirmPassword = "";
      });
  },

  async GetDetails({ commit, dispatch }) {
    return await this.$axios
      .get("/account/details")
      .then(function(response) {
        commit("SET_ACCOUNT_DETAILS", response.data);
      })
      .catch(function(error) {
        return HandleError(error, dispatch);
      });
  },

  async UpdateDetails({ dispatch }) {
    return await this.$axios
      .post("/account/update", state.account)
      .then(function() {
        dispatch("addNotification", {
          msg: "Successfully Updated Details",
          type: "success",
        });
      })
      .catch(function(error) {
        return HandleError(error, dispatch);
      });
  },

  async DeleteAccount({ commit, dispatch }) {
    return await this.$axios
      .delete("/account/delete")
      .then(function() {
        dispatch("addNotification", {
          msg: "Successfully Deleted User",
          type: "success",
        });
        commit("CLEAR_ACCOUNT_DATA");
      })
      .catch(function(error) {
        return HandleError(error, dispatch);
      });
  },

  logout({ commit }) {
    commit("CLEAR_ACCOUNT_DATA");
  },

  checkSession({ commit }) {
    if (localStorage.getItem("account")) {
      commit("CHECK_SESSION");
    }
  },
};

const mutations = {
  SET_ACCOUNT(state, account) {
    state.account.roles = account.roles;
    state.account.token = account.token;
    state.account.authenticated = true;
    localStorage.setItem("account", JSON.stringify(state.account));
    this.$axios.defaults.headers.common[
      "authorization"
    ] = `Bearer ${account.token}`;
  },

  SET_AUTH_TOKEN(state, token) {
    state.account.token = token;
    this.$axios.defaults.headers.common["authorization"] = `Bearer ${token}`;
  },

  CLEAR_ACCOUNT_DATA(state) {
    state.account = new account();
    localStorage.removeItem("account");
    this.$axios.defaults.headers.common["authorization"] = null;
  },

  CHECK_SESSION(state) {
    state.account = JSON.parse(localStorage.getItem("account"));
    this.$axios.defaults.headers.common[
      "authorization"
    ] = `Bearer ${state.account.token}`;
  },

  SET_ACCOUNT_DETAILS(state, account) {
    state.account.userName = account.userName;
    state.account.email = account.email;
    state.account.firstName = account.firstName;
    state.account.lastName = account.lastName;
    state.account.phoneNumber = account.phoneNumber;
  },
};

function HandleError(error, dispatch) {
  if (error.response) {
    // The request was made and the server responded with a status code
    var errors = error.response.data.errors;
    //When the model state is invalid we need to parse the response error object differently
    if (Array.isArray(errors)) {
      errors.forEach((message) => {
        dispatch("addNotification", {
          msg: message,
          type: "error",
        });
      });
    } else {
      for (var e in errors) {
        errors[e].forEach((message) => {
          dispatch("addNotification", {
            msg: message,
            type: "error",
          });
        });
      }
    }
  } else if (error.request) {
    // The request was made but no response was received
    dispatch("addNotification", {
      msg: "Failed To Contact Server",
      type: "error",
    });
  } else {
    // Something happened in setting up the request that triggered an Error
    dispatch("addNotification", {
      msg: "Failed Sending Request",
      type: "error",
    });
  }
  return Promise.reject(error);
}

export default {
  state,
  getters,
  actions,
  mutations,
};
