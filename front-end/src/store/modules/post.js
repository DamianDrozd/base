class Post {
  constructor() {
    this.postId = 0;
    this.username = "";
    this.title = "";
    this.thumbnail = null,
    this.content = "";
  }
}

const state = {
  post: new Post(),
  posts: [],
};

const getters = {
  post: (state) => state.post,
  posts: (state) => state.posts,
  postContent: (state) => {
    return JSON.parse(state.post.content)
  }
};

const actions = {
  async CreatePost({ commit, dispatch }) {
    var formData = new FormData();
    formData.append("Title", state.post.title);
    formData.append("Thumbnail", state.post.thumbnail);
    formData.append("Content", state.post.content);

    return await this.$axios
      .post("/posts/create", formData, {headers: {'Content-Type': 'multipart/form-data'}})
      .then(function() {
        dispatch("addNotification", {
          msg: "Successfully Created Post",
          type: "success",
        });
        commit("CLEAR_POST");
      })
      .catch(function(error) {
        return HandleError(error, dispatch);
      });
  },
  async GetPost({ commit, dispatch }, postId) {
    return await this.$axios
      .get("/posts/get/id/" + postId)
      .then(function(response) {
        commit("SET_POST", response.data);
      })
      .catch(function(error) {
        return HandleError(error, dispatch);
      });
  },
  async GetPosts({ commit, dispatch }) {
    return await this.$axios
      .get("/posts/get")
      .then(function(response) {
        commit("SET_POSTS", response.data);
      })
      .catch(function(error) {
        return HandleError(error, dispatch);
      });
  },
  async GetPostsUser({ commit, dispatch }, userId) {
    return await this.$axios
      .get("/posts/get/username/" + userId)
      .then(function(response) {
        commit("SET_POSTS", response.data);
      })
      .catch(function(error) {
        return HandleError(error, dispatch);
      });
  },
  async UpdatePost({ dispatch }, postId) {
    var formData = new FormData();
    formData.append("PostId", state.post.postId);
    formData.append("Username", state.post.username);
    formData.append("Title", state.post.title);
    formData.append("Thumbnail", state.post.thumbnail);
    formData.append("Content", state.post.content);

    return await this.$axios
      .put("/posts/update/" + postId, formData, {headers: {'Content-Type': 'multipart/form-data'}})
      .then(function() {
        dispatch("addNotification", {
          msg: "Successfully Updated Post",
          type: "success",
        });
      })
      .catch(function(error) {
        return HandleError(error, dispatch);
      });
  },
  ClearPost({ commit }) {
    commit("CLEAR_POST");
  },
};

const mutations = {
  CLEAR_POST(state) {
    state.post = new Post();
  },
  SET_POST(state, post) {
    state.post = post;
  },
  SET_POSTS(state, posts) {
    state.posts = posts;
  },
  SET_POST_USERNAME(state, username) {
    state.post.username = username;
  },
};

function HandleError(error, dispatch) {
  if (error.response) {
    // The request was made and the server responded with a status code
    var errors = error.response.data.errors;
    //When the model state is invalid we need to parse the response error object differently
    if (Array.isArray(errors)) {
      errors.forEach((message) => {
        dispatch("addNotification", {
          msg: message,
          type: "error",
        });
      });
    } else {
      for (var e in errors) {
        errors[e].forEach((message) => {
          dispatch("addNotification", {
            msg: message,
            type: "error",
          });
        });
      }
    }
  } else if (error.request) {
    // The request was made but no response was received
    dispatch("addNotification", {
      msg: "Failed To Contact Server",
      type: "error",
    });
  } else {
    // Something happened in setting up the request that triggered an Error
    dispatch("addNotification", {
      msg: "Failed Sending Request",
      type: "error",
    });
  }
  return Promise.reject(error);
}

export default {
  state,
  getters,
  actions,
  mutations,
};
