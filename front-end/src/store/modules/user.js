class user {
    constructor() {
        this.email = "";
        this.username = "";
        this.password = "";
        this.confirmPassword = "";
        this.firstName = "";
        this.lastName = "";
        this.phoneNumber = "";
        this.twoFactorEnabled = false;
        this.roles = [];
    }
}

const state = {
    user: new user(), 
    users: [],
    roles: []
};

const getters = {
    user: state => state.user,
    users: state => state.users,
    userRoles: state => state.roles
};

const actions = {
    async CreateUser({ commit, dispatch }) {
        return await this.$axios.post('/user/create', state.user)
            .then(function () {
                dispatch('addNotification', { msg: 'Successfully Create User', type: 'success' });
                commit('CLEAR_USER');
            })
            .catch(function (error) {
                return HandleError(error, dispatch);
            })
    },
    async GetUser({ commit, dispatch }, userName) {
        return await this.$axios.get(('user/get/' + userName))
            .then(function (response) {
                commit('SET_USER', response.data);
            })
            .catch(function (error) {
                return HandleError(error, dispatch);
            })
    },
    async SearchUser({ commit, dispatch }, searchText) {
        return await this.$axios.get(('user/search/' + searchText))
            .then(function (response) {
                commit('SET_USERS', response.data);
            })
            .catch(function (error) {
                return HandleError(error, dispatch);
            })
    },
    async GetRoles({ commit, dispatch }) {
        return await this.$axios.get('/user/roles')
            .then(function (response) {
                commit('SET_USER_ROLES', response.data.roles);
            })
            .catch(function (error) {
                return HandleError(error, dispatch);
            })
    }    
};

const mutations = {
    SET_USER_ROLES(state, roles) {
        state.roles = [];
        roles.forEach(role => {
            state.roles.push(role);
        });
    },
    CLEAR_USER(state) {
        state.user = new user();
    },
    SET_USERS(state, users){
        state.users = users;
    },
    SET_USER(state, user){
        state.user = user;
        // state.user.email = _user.email;
        // state.user.userName = _user.userName;
        // state.user.firstName = _user.firstName;
        // state.user.lastName = _user.lastName;
        // state.user.phoneNumber = _user.phoneNumber;
        // state.user.twoFactorEnabled = _user.twoFactorEnabled;
        // state.user.roles = _user.roles;
    }
};

function HandleError(error, dispatch) {
  if (error.response) {
    // The request was made and the server responded with a status code
    var errors = error.response.data.errors;
    //When the model state is invalid we need to parse the response error object differently
    if (Array.isArray(errors)) {
      errors.forEach((message) => {
        dispatch("addNotification", {
          msg: message,
          type: "error",
        });
      });
    } else {
      for (var e in errors) {
        errors[e].forEach((message) => {
          dispatch("addNotification", {
            msg: message,
            type: "error",
          });
        });
      }
    }
  } else if (error.request) {
    // The request was made but no response was received
    dispatch("addNotification", {
      msg: "Failed To Contact Server",
      type: "error",
    });
  } else {
    // Something happened in setting up the request that triggered an Error
    dispatch("addNotification", {
      msg: "Failed Sending Request",
      type: "error",
    });
  }
  return Promise.reject(error);
}

export default {
    state,
    getters,
    actions,
    mutations
};