class notification {
    constructor(id, msg, type) {
        this.id = id;
        this.msg = msg;
        this.type = type;
    }
}

let Id = 1;

const state = {
    notifications: []
};

const getters = {
    notifications: state => state.notifications
};

const actions = {
    addNotification({commit}, _msg, type) {
        commit("ADD_NOTIFICATION", _msg, type);
    },
    removeNotification({commit}, id) {
        commit("REMOVE_NOTIFICATION", id);
    }
};

const mutations = {
    ADD_NOTIFICATION: (state, payload) => {
        state.notifications.push(new notification(Id, payload.msg, payload.type));
        Id += 1;
    },
    REMOVE_NOTIFICATION: (state, id) => {
        state.notifications = state.notifications.filter(notification => notification.id !== id);
    }
};

export default {
    state,
    getters,
    actions,
    mutations
};