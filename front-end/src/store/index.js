import Vuex from "vuex";
import Vue from "vue";
import account from "./modules/account";
import notifications from "./modules/notifications";
import user from "./modules/user";
import post from "./modules/post"

Vue.use(Vuex);

export default new Vuex.Store({
    modules: {
        account,
        user,
        notifications,
        post
    }
});