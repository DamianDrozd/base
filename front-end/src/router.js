import Vue from "vue";
import Router from "vue-router";
// import store from './store';

Vue.use(Router);

var router = new Router({
  mode: "history",
  routes: [
    //Main Views
    {
      path: "/",
      name: "Home",
      component: () => import("./views/main/Home.vue"),
      meta: {
        pageTitle: "Home",
        navButtonText: "HOME",
        allowAnonymous: false,
        authenticate: false,
        authorize: ["*"],
      },
    },
    {
      path: "/posts/",
      component: () => import("./views/main/posts/Posts.vue"),
      meta: {
        navButtonText: "POSTS",
        allowAnonymous: false,
        authenticate: true,
        authorize: ["User"],
      },
      children: [
        {
          path: "create",
          component: () => import("./views/main/posts/PostsCreate.vue"),
          meta: {
            pageTitle: "Post Workbench",
            navButtonText: "CREATE",
            allowAnonymous: false,
            authenticate: true,
            authorize: ["User"],
          },
        },
        {
          path: "details/:postId",
          component: () => import("./views/main/posts/PostsDetails.vue"),
          meta: {
            pageTitle: "Post Details",
            allowAnonymous: true,
            authenticate: false,
            authorize: ["*"],
          },
        },
        {
          path: ":userName",
          component: () => import("./views/main/posts/PostsListUser.vue"),
          meta: {
            pageTitle: "User Posts",
            navButtonText: "My POSTS",
            allowAnonymous: false,
            authenticate: true,
            authorize: ["User"],
          },
        },
        {
          path: "update/:postId",
          component: () => import("./views/main/posts/PostsUpdate.vue"),
          meta: {
            pageTitle: "Post Workbench",
            allowAnonymous: false,
            authenticate: true,
            authorize: ["User"],
          },
        },
      ],
    },
    {
      path: "/account/",
      component: () => import("./views/main/account/Account.vue"),
      meta: {
        navButtonText: "ACCOUNT",
        allowAnonymous: false,
        authenticate: true,
        authorize: ["User"],
      },
      children: [
        {
          path: "password/reset/",
          name: "ForgotPassword",
          component: () => import("./views/main/account/PasswordResetSend.vue"),
          meta: {
            pageTitle: "Forgot Password",
            allowAnonymous: true,
            authenticate: false,
            authorize: ["*"],
          },
        },
        {
          path: "password/reset/:token/:userName",
          component: () => import("./views/main/account/PasswordReset.vue"),
          meta: {
            pageTitle: "Forgot Password",
            allowAnonymous: true,
            authenticate: false,
            authorize: ["*"],
          },
        },
        {
          path: "email/confirm/:token",
          component: () => import("./views/main/account/EmailConfirm.vue"),
          meta: {
            pageTitle: "Email Confirmation",
            allowAnonymous: false,
            authenticate: true,
            authorize: ["User"],
          },
        },
        {
          path: "details",
          component: () => import("./views/main/account/Details.vue"),
          meta: {
            pageTitle: "My Account",
            navButtonText: "Details",
            allowAnonymous: false,
            authenticate: true,
            authorize: ["User"],
          },
        },
      ]
    },
    //Admin Views
    {
      path: "/admin/",
      component: () => import("./views/admin/Admin.vue"),
      meta: {
        pageTitle: "Admin Login",
        navButtonText: "ADMIN",
        allowAnonymous: false,
        authenticate: true,
        authorize: ["Admin"],
      },
      children: [
        {
          path: "user-maintenance",
          name: "UserMaintenance",
          component: () => import("./views/admin/UserMaintenance.vue"),
          meta: {
            pageTitle: "User Maintenance",
            navButtonText: "USER MAINTENANCE",
            allowAnonymous: false,
            authenticate: true,
            authorize: ["Admin"],
          },
        },
      ]
    },
    //General Views
    {
      path: "/about",
      name: "About",
      component: () => import("./views/general/about.vue"),
      meta: {
        pageTitle: "About",
        navButtonText: "ABOUT",
        allowAnonymous: true,
        authenticate: false,
        authorize: ["*"],
      },
    },
    {
      path: "/register",
      name: "Register",
      component: () => import("./views/main/account/Register.vue"),
      meta: {
        pageTitle: "Register",
        navButtonText: "REGISTER",
        allowAnonymous: false,
        authenticate: false,
        authorize: ["*"],
      },
    },
    {
      path: "/login",
      name: "Login",
      component: () => import("./views/main/account/Login.vue"),
      meta: {
        pageTitle: "Login",
        navButtonText: "LOGIN",
        allowAnonymous: false,
        authenticate: false,
        authorize: ["*"],
      },
    },
    {
      path: "*",
      name: "404",
      component: () => import("./views/general/404.vue"),
      meta: {
        pageTitle: "Page Not Found",
        allowAnonymous: true,
        authenticate: false,
        authorize: ["*"],
      },
    },
  ],
});

// router.beforeEach((to, from, next) => {
//   //Redirect back to login if user if not logged in or authorized
//   //Check if authentication is required and user login status
//   if ((to.meta.authenticate && !store.getters.loggedIn)
//   //Check if user is authorized by role 
//   || !(to.meta.authorize.includes('*') || to.meta.authorize.some(role => store.getters.account.roles.includes(role)))) {
//     next({ path: '/', query: { redirect: to.fullPath } });
//   }else{
//     next();
//   }
// });

export default router;
