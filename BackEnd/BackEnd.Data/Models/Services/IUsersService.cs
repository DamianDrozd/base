﻿using BackEnd.Core.Models.ViewModels.UserViewModels;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BackEnd.Data.Models.Services
{
    public interface IUsersService
    {
        Task Create(UserCreateViewModel _user);

        Task<UserViewModel> GetByUserName(string userName);

        Task<List<UserViewModel>> SearchUsers(string searchText);

        Task Update(UserViewModel model);

        UserRolesViewModel GetUserRoles();
    }
}
