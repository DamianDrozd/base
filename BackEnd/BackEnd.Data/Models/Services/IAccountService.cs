﻿using BackEnd.Core.Models.ViewModels.AccountViewModels;
using BackEnd.Data.Models.Identity;
using System.Threading.Tasks;


namespace BackEnd.Data.Models.Services
{
    public interface IAccountService
    {
        //Create the user, send a verification email and generate a session
        Task<AccountTokenModel> Register(AccountRegisterViewModel account);

        //Create a user based on an external login provider and start a session
        Task<AccountTokenModel> RegisterExternal(AccountExternalModel model);

        //Reset password for a user
        Task PasswordReset(string userName, string token, string newPassword);

        //Confirm a users email. User must eb signed in.
        Task EmailConfirm(string userId, string token);

        //Find the user and sned confirmation email
        Task EmailConfirmSend(string userId);

        //Login into an account, fetch relevant user data, and start jwt session
        Task<AccountTokenModel> Login(AccountLoginViewModel account);

        //Login into an external users account, fetch relevant data, and start jwt session
        Task<AccountTokenModel> LoginExternal(AccountExternalModel model);

        //Get account details by id
        Task<AccountDetailsViewModel> Details(string userId);

        //Update account by id
        Task Update(AccountDetailsViewModel _account, string userId);

        //Delete a user by id
        Task Delete(string userId);

        //Email a password reset link if a user exists
        Task SendPasswordReset(string id);

        //Get the users post view history
        //Task GetUserHistory(string userId);
    }
}