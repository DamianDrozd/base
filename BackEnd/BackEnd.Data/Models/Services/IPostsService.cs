﻿using BackEnd.Core.Models.ViewModels.PostViewModels;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BackEnd.Data.Models.Services
{
    public interface IPostsService : IUserHistoryService
    {
        //Create a new post
        Task Create(PostViewModelCreate _post, string userId);

        //Get a post specified by id
        Task<PostViewModel> GetPostByPostId(int postId);

        //Get All posts
        Task<List<PostViewModel>> GetPosts();

        //Get all posts associated to user
        Task<List<PostViewModel>> GetUserPosts(string userName);

        //Update post details
        Task Update(int postId, PostViewModelUpdate _post, string userId);
    }
}
