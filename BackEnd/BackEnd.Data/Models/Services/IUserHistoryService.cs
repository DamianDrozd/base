﻿using BackEnd.Core.Models.ViewModels.PostViewModels;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BackEnd.Data.Models.Services
{
    public interface IUserHistoryService
    {
        //Add posts to a users view history when a user opens a post
        Task AddPostToUserHistory(string userId, int postId);

        //Get users post view history
        Task<List<PostUserHistory>> GetUserHistory(string userId);
    }
}
