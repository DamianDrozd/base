﻿using BackEnd.Data.Models.App;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace BackEnd.Data.Models.Identity
{
    public class AppUser : IdentityUser<Guid>
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }

        [InverseProperty("User")]
        public ICollection<Post> Posts { get; set; }

        [InverseProperty("User")]
        public ICollection<UserHistory> UserHistory { get; set; }
    }
}
