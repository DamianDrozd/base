﻿using BackEnd.Data.Models.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace BackEnd.Data.Models.App
{
    public class Post
    {
        [Key]
        public int PostId { get; set; }
        public string Title { get; set; }
        public string Thumbnail { get; set; }
        public string Content { get; set; }
        public DateTime CreateDateTime { get; set; }
        public DateTime UpdateDateTime { get; set; }

        public Guid UserId { get; set; }
        [ForeignKey("UserId")]
        public AppUser User { get; set; }

        [InverseProperty("Post")]
        public ICollection<UserHistory> UserHistory { get; set; }
    }
}
