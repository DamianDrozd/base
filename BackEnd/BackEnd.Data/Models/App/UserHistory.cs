﻿using BackEnd.Data.Models.Identity;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace BackEnd.Data.Models.App
{
    public class UserHistory
    {
        [Key]
        public int Id { get; set; }
        public DateTime ViewDate { get; set; }

        public Guid UserId { get; set; }
        [ForeignKey("UserId")]
        public AppUser User { get; set; }

        public int PostId { get; set; }
        [ForeignKey("PostId")]
        public Post Post { get; set; }
    }
}
