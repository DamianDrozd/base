﻿using BackEnd.Data;
using BackEnd.Data.Models.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace BackEnd.Data
{
    class AppDBContextFactory : IDesignTimeDbContextFactory<AppDbContext>
    {
        public AppDBContextFactory()
        {

        }

        public AppDbContext CreateDbContext(string[] args)
        {
            IConfiguration Configuration = new ConfigurationBuilder()
                .SetBasePath(Path.Combine(Directory.GetCurrentDirectory(), "../BackEnd.API"))
                .AddJsonFile("appsettings.Development.json")
                .AddEnvironmentVariables()
                .Build();

            var builder = new DbContextOptionsBuilder<AppDbContext>();
            builder.UseNpgsql(Environment.GetEnvironmentVariable("BaseDbConnection"));

            return new AppDbContext(builder.Options);
        }
    }
}
