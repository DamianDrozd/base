﻿using BackEnd.Data.Models.App;
using BackEnd.Data.Models.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;

namespace BackEnd.Data
{
    public class AppDbContext : IdentityDbContext<AppUser, AppRole, Guid>
    {
        public DbSet<Post> Posts { get; set; }
        public DbSet<UserHistory> UserHistory { get; set; }
        public AppDbContext(DbContextOptions<AppDbContext> options) : base(options)
        {
            
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            //Rename Identity user Id column to UserId
            builder.Entity<AppUser>(e => e.Property(p => p.Id).HasColumnName("UserId"));

            ////User Post - 1 to many
            //builder.Entity<AppUser>().HasMany(e => e.Posts).WithOne(e => e.User);

            ////User UserHistory - 1 to many
            //builder.Entity<AppUser>().HasMany(e => e.UserHistory).WithOne(e => e.User);

            ////Post UserHistory - 1 to many
            //builder.Entity<Post>().HasMany(e => e.UserHistory).WithOne(e => e.Post);
        }

    }
}
