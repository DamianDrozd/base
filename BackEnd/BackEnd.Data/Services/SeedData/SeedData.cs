﻿using BackEnd.Data.Models.Identity;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.DependencyInjection;
using System;

namespace BackEnd.Data.Services.SeedData
{
    public class SeedData
    {
        public static async void Initialize(IServiceProvider serviceProvider)
        {
            const string RoleAdminName = "Admin";
            const string RoleUserName = "User";

            AppUser defaultUser;

            var Context = serviceProvider.GetService<AppDbContext>();
            var UserManager = serviceProvider.GetService<UserManager<AppUser>>();
            var RoleManager = serviceProvider.GetService<RoleManager<AppRole>>();

            Context.Database.EnsureCreated();

            //Create default roles
            if (!await RoleManager.RoleExistsAsync(RoleAdminName))
            {
                var role = new AppRole();
                role.Name = RoleAdminName;
                await RoleManager.CreateAsync(role);

            }

            //Create default roles
            if (!await RoleManager.RoleExistsAsync(RoleUserName))
            {
                var role = new AppRole();
                role.Name = RoleUserName;
                await RoleManager.CreateAsync(role);

            }

            //Create default users
            defaultUser = await UserManager.FindByNameAsync("Damso");
            if (defaultUser == null)
            {
                defaultUser = new AppUser()
                {
                    Email = "damian.drozd@hotmail.com",
                    SecurityStamp = Guid.NewGuid().ToString(),
                    UserName = "Damso",
                };
                await UserManager.CreateAsync(defaultUser, "123D@mian95");
            }

            //Add admin role and user to default user
            var rolesForUser = await UserManager.GetRolesAsync(defaultUser);
            if (!rolesForUser.Contains(RoleAdminName))
            {
                await UserManager.AddToRoleAsync(defaultUser, RoleAdminName);
            }
            if (!rolesForUser.Contains(RoleUserName))
            {
                await UserManager.AddToRoleAsync(defaultUser, RoleUserName);
            }
        }
    }
}
