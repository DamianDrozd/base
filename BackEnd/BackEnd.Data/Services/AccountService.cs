﻿using BackEnd.Data.Models.Identity;
using BackEnd.Core.Models.ViewModels.AccountViewModels;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Configuration;
using System;
using System.Linq;
using System.Threading.Tasks;
using BackEnd.Core.Utilities;
using System.Collections.Generic;
using System.Net;
using Google.Apis.Auth;
using BackEnd.Data.Models.Services;

namespace BackEnd.Data.Services
{
    public class AccountService : IAccountService {

        private const string RoleUser = "User";

        private readonly IConfiguration _configuration;
        private UserManager<AppUser> _userManager;
        private SignInManager<AppUser> _signInManager;

        //Error messages for exception throws
        private const string _errMessageCreateUserFailed = "Failed to create new user.";
        private const string _errMessageSignInFailed = "Invalid id or password.";
        private const string _errMessageUserNotFound = "Failed to find user.";
        private const string _errMessageUpdateFailed = "Failed to update.";
        private const string _errMessageDeleteFailed = "Failed to delete.";
        private const string _errMessageConfirmEmailFailed = "Failed to confirm email.";
        private const string _errMessagePasswordResetFailed = "Failed to reset password.";
        private const string _errMessageValidateGoogleUserFail = "Failed to validate the Google user.";
        public AccountService()
        {
            
        }

        public AccountService(IConfiguration configuration, UserManager<AppUser> userManager, SignInManager<AppUser> signInManager)
        {
            _configuration = configuration;
            _userManager = userManager;
            _signInManager = signInManager;
        }

        //Create the user, send a verification email and generate a session
        public async Task<AccountTokenModel> Register(AccountRegisterViewModel account)
        {
            //create user and add relevant roles
            var user = new AppUser { UserName = account.UserName, Email = account.Email, PhoneNumber = account.PhoneNumber, FirstName = account.FirstName, LastName = account.LastName, TwoFactorEnabled = false };
            var result = await _userManager.CreateAsync(user, account.Password);
            if (result.Succeeded)
            {
                result = await _userManager.AddToRoleAsync(user, RoleUser);
                if (result.Succeeded)
                {
                    //If valid login, send verification email and start session
                    if (await IsValidLoginAsync(user.UserName, account.Password))
                    {
                        await SendConfirmationEmail(user);

                        return await GetJwtToken(user);
                    }
                    await Delete(user.Id.ToString());
                    throw new Exception(_errMessageSignInFailed);
                }
            }
            throw new Exception(_errMessageCreateUserFailed);
        }

        //Create a user based on an external login provider and start a session
        public async Task<AccountTokenModel> RegisterExternal(AccountExternalModel model)
        {
            //Validate token
            var user = await GetExternalUser(model.Provider, model.Token);
            if (user != null)
            {
                //Create a user, add external login record, and add user to relevant roles
                var result = await _userManager.CreateAsync(user);
                if (result.Succeeded)
                {
                    var userInfo = new UserLoginInfo(model.Provider, user.UserName, model.Provider.ToUpperInvariant());
                    result = await _userManager.AddLoginAsync(user, userInfo);
                    if (result.Succeeded)
                    {
                        result = await _userManager.AddToRoleAsync(user, RoleUser);
                        if (result.Succeeded)
                        {
                            //Send verification email and start session
                            await SendConfirmationEmail(user);

                            return await GetJwtToken(user);
                        }
                        await _userManager.RemoveLoginAsync(user, model.Provider, user.UserName);
                    }
                    await Delete(user.Id.ToString());
                }
            }
            throw new Exception(_errMessageCreateUserFailed);
        }

        //Reset password for a user
        public async Task PasswordReset(string userName, string token, string newPassword)
        {
            var user = await _userManager.FindByNameAsync(userName);
            if (user != null)
            {
                var response = await _userManager.ResetPasswordAsync(user, WebUtility.UrlDecode(token), newPassword);
                if (!response.Succeeded)
                {
                    throw new Exception(_errMessagePasswordResetFailed);
                }
            }
            else
            {
                throw new Exception(_errMessageUserNotFound);
            }
        }

        //Confirm a users email. User must eb signed in.
        public async Task EmailConfirm(string userId, string token)
        {
            var user = await _userManager.FindByIdAsync(userId);
            if(user != null)
            {
                //var response = await _userManager.ConfirmEmailAsync(user, WebUtility.UrlDecode(token));
                var response = await _userManager.ConfirmEmailAsync(user, token);
                if (!response.Succeeded)
                {
                    throw new Exception(_errMessageConfirmEmailFailed);
                }
            }
            else
            {
                throw new Exception(_errMessageConfirmEmailFailed);
            }
        }

        //Find the user and sned confirmation email
        public async Task EmailConfirmSend(string userId)
        {
            var user = await _userManager.FindByIdAsync(userId);
            if (user != null)
            {
                await SendConfirmationEmail(user);
            }
            else
            {
                throw new Exception(_errMessageUserNotFound);
            }
        }

        //Login into an account, fetch relevant user data, and start jwt session
        public async Task<AccountTokenModel> Login(AccountLoginViewModel account)
        {
            if (await IsValidLoginAsync(account.UserName, account.Password))
            {
                var user = await _userManager.FindByNameAsync(account.UserName);
                if (user != null)
                {
                    return await GetJwtToken(user);
                }
            }
            throw new Exception(_errMessageSignInFailed);
        }

        //Login into an external users account, fetch relevant data, and start jwt session
        public async Task<AccountTokenModel> LoginExternal(AccountExternalModel model)
        {
            var externalUser = await GetExternalUser(model.Provider, model.Token);
            if (externalUser != null)
            {
                var user = await _userManager.FindByLoginAsync(model.Provider, externalUser.UserName);
                if (user != null)
                {
                    return await GetJwtToken(user);
                }
            }
            throw new Exception(_errMessageSignInFailed);
        }

        //Get account details by id
        public async Task<AccountDetailsViewModel> Details(string userId)
        {
            var user = await _userManager.FindByIdAsync(userId);
            if (user != null) 
            {
                return new AccountDetailsViewModel()
                {
                    UserName = user.UserName,
                    Email = user.Email,
                    FirstName = user.FirstName,
                    LastName = user.LastName,
                    PhoneNumber = user.PhoneNumber,
                    TwoFactorEnabled = user.TwoFactorEnabled
                };
            }
            throw new Exception(_errMessageUserNotFound);
        }

        //Update account by id
        public async Task Update(AccountDetailsViewModel _account, string userId)
        {
            var user = await _userManager.FindByIdAsync(userId);
            if (user != null)
            {
                user.FirstName = _account.FirstName;
                user.LastName = _account.LastName;
                user.PhoneNumber = _account.PhoneNumber;
                user.TwoFactorEnabled = _account.TwoFactorEnabled;

                var result = await _userManager.UpdateAsync(user);
                if (!result.Succeeded)
                {
                    throw new Exception(_errMessageUpdateFailed);
                }
            }
            else
            {
                throw new Exception(_errMessageUserNotFound);
            }
        }

        //Delete a user by id
        public async Task Delete(string userId)
        {
            var user = await _userManager.FindByIdAsync(userId);
            if (user != null)
            {
                var result = await  _userManager.DeleteAsync(user);
                if (!result.Succeeded) {
                    throw new Exception(_errMessageDeleteFailed);
                }
            }
            else
            {
                throw new Exception(_errMessageUserNotFound);
            }
        }

        //Email a password reset link if a user exists
        public async Task SendPasswordReset(string id)
        {
            var user = await _userManager.FindByEmailAsync(id);
            if (user == null)
            {
                user = await _userManager.FindByNameAsync(id);
            }
            if (user != null)
            {
                var token = await _userManager.GeneratePasswordResetTokenAsync(user);
                var passwordResetLinkLink = _configuration["HttpProtocol"] + "://" + _configuration["Host"] + _configuration["Links:AccountPasswordReset"] + WebUtility.UrlEncode(token) + "/" + user.UserName;
                try
                {
                    var apiKey = _configuration["Environment"].Equals("Development") ? _configuration["Keys:SendGridApiKey"] : Environment.GetEnvironmentVariable("SendGridApiKey");
                    var emailUtility = new EmailUtility(apiKey);
                    //create data dictionary for substitute variables in sendgrid template
                    var templateData = new Dictionary<string, string>
                    {
                        {"Password_Reset_Link", passwordResetLinkLink}
                    };
                    await emailUtility.SendEmailByTemplate(
                        _configuration["Email:NoReplyEmail"],
                        _configuration["Email:NoReplyName"],
                        user.Email,
                        user.FirstName,
                        _configuration["Email:TemplateId:PasswordReset"]
                        , templateData
                    );
                }
                catch (Exception exception)
                {
                    throw new Exception(exception.Message);
                }
            }
        }

        //Create a verification token and confirmation link and send to user email
        private async Task SendConfirmationEmail(AppUser user)
        {
            var token = await _userManager.GenerateEmailConfirmationTokenAsync(user);
            var confirmationLink = _configuration["HttpProtocol"] + "://" + _configuration["Host"] + _configuration["Links:AccountEmailConfirm"] + WebUtility.UrlEncode(token);
            try
            {
                var apiKey = _configuration["Environment"].Equals("Development") ? _configuration["Keys:SendGridApiKey"] : Environment.GetEnvironmentVariable("SendGridApiKey");
                var emailUtility = new EmailUtility(apiKey);
                //create data dictionary for substitute variables in sendgrid template
                var templateData = new Dictionary<string, string>
                {
                    {"Email_Confirmation_Link", confirmationLink}
                };
                await emailUtility.SendEmailByTemplate(
                    _configuration["Email:NoReplyEmail"],
                    _configuration["Email:NoReplyName"],
                    user.Email,
                    user.FirstName,
                    _configuration["Email:TemplateId:EmailConfirm"]
                    , templateData
                );
            }
            catch (Exception exception)
            {
                throw new Exception(exception.Message);
            }
        }
        
        private async Task<bool> IsValidLoginAsync(string username, string password)
        {
            var result = await _signInManager.PasswordSignInAsync(username, password, false, false);
            return result.Succeeded;
        }

        private async Task<AccountTokenModel> GetJwtToken(AppUser user)
        {
            var roles = await _userManager.GetRolesAsync(user);
            var token = new JwtService(_configuration).GenerateSecurityToken(user, roles);
            return new AccountTokenModel
            {
                Token = token,
                Roles = roles.ToList()
            };
        }

        private async Task<AppUser> GetExternalUser(string provider, string token)
        {
            AppUser user = null;
            switch (provider)
            {
                case "google":
                    GoogleJsonWebSignature.Payload googleUser;
                    try
                    {
                        googleUser = await GoogleJsonWebSignature.ValidateAsync(token, new GoogleJsonWebSignature.ValidationSettings()
                        {
                            Audience = new[] { _configuration["Keys:GoogleOAuthClientId"] }
                        });

                        user = new AppUser()
                        {
                            UserName = googleUser.Subject,
                            Email = googleUser.Email,
                            FirstName = googleUser.GivenName,
                            LastName = googleUser.FamilyName
                        };
                    }
                    catch (Exception exception)
                    {
                        throw exception;
                    }
                    break;
            }

            return user;
        }
    }
}
