﻿using BackEnd.Data.Models.Identity;
using BackEnd.Core.Models.ViewModels.UserViewModels;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BackEnd.Data.Models.Services;

namespace BackEnd.Data.Services
{
    public class UsersService : IUsersService
    {
        private UserManager<AppUser> _userManager;
        private RoleManager<AppRole> _roleManager;

        //Error messages for exception throws
        private const string _errMessageCreateUserFailed = "Failed to create new user.";
        private const string _errMessageAddRoleFailed = "Failed to add user roles.";
        private const string _errMessageUserNotFound = "Failed to find user.";
        private const string _errMessageUsersNotFound = "Failed to retrieve users.";
        private const string _errMessageUpdateUserFailed = "Failed to update user.";

        public UsersService(UserManager<AppUser> userManager, RoleManager<AppRole> roleManager)
        {
            _userManager = userManager;
            _roleManager = roleManager;
        }

        public async Task Create(UserCreateViewModel _user)
        {
            var user = new AppUser { UserName = _user.UserName, Email = _user.Email, PhoneNumber = _user.PhoneNumber, FirstName = _user.FirstName, LastName = _user.LastName, TwoFactorEnabled = false };
            var result = await _userManager.CreateAsync(user, _user.Password);
            if (result.Succeeded)
            {
                foreach (string role in _user.Roles)
                {
                    result = await _userManager.AddToRoleAsync(user, role);
                    if (!result.Succeeded)
                    {
                        throw new Exception(_errMessageAddRoleFailed);
                    }
                }
            }
            else
            {
                throw new Exception(_errMessageCreateUserFailed);
            }
        }

        public async Task<UserViewModel> GetByUserName(string userName)
        {
            var user = await _userManager.FindByNameAsync(userName);
            if (user != null)
            {
                var roles = _userManager.GetRolesAsync(user).Result;
                return new UserViewModel()
                {
                    Username = user.UserName,
                    Email = user.Email,
                    FirstName = user.FirstName,
                    LastName = user.LastName,
                    PhoneNumber = user.PhoneNumber,
                    TwoFactorEnabled = user.TwoFactorEnabled,
                    Roles = roles
                };
            }
            throw new Exception(_errMessageUserNotFound);
        }

        public async Task<List<UserViewModel>> SearchUsers(string searchText)
        {
            searchText = searchText.ToUpper();
            var users = new List<UserViewModel>();
            var userList = await _userManager.Users.Where(user => user.NormalizedUserName.Contains(searchText) || user.NormalizedEmail.Contains(searchText) || user.FirstName.ToUpper().Contains(searchText) || user.LastName.ToUpper().Contains(searchText) || user.PhoneNumber.Contains(searchText)).ToListAsync();

            if (userList != null)
            {
                foreach (var user in userList)
                {
                    var roles = _userManager.GetRolesAsync(user).Result;
                    users.Add(new UserViewModel()
                    {
                        Username = user.UserName,
                        Email = user.Email,
                        FirstName = user.FirstName,
                        LastName = user.LastName,
                        PhoneNumber = user.PhoneNumber,
                        Roles = roles,
                        TwoFactorEnabled = user.TwoFactorEnabled
                    });
                }
                return users;
            }
            throw new Exception(_errMessageUsersNotFound);
        }

        public async Task Update(UserViewModel model)
        {
            var user = await _userManager.FindByNameAsync(model.Username);
            if (user != null)
            {
                user.Email = model.Email;
                user.FirstName = model.FirstName;
                user.LastName = model.LastName;
                user.PhoneNumber = model.PhoneNumber;
                user.TwoFactorEnabled = model.TwoFactorEnabled;
                //var roles = _roleManager.Roles.Select(role => role.Name).AsEnumerable();
                var result = _userManager.RemoveFromRolesAsync(user, _roleManager.Roles.Select(role => role.Name).AsEnumerable());
                if (result.IsCompletedSuccessfully)
                {
                    foreach (var role in model.Roles)
                    {
                        await _userManager.AddToRoleAsync(user, role);
                    }
                }
            }
            else
            {
                throw new Exception(_errMessageUpdateUserFailed);
            }
        }

        public UserRolesViewModel GetUserRoles()
        {
            var model = new UserRolesViewModel()
            {
                Roles = new List<string>()
            };
            var result = _roleManager.Roles;
            if(result != null)
            {
                foreach (AppRole role in result)
                {
                    model.Roles.Add(role.Name);
                }
                return model;
            }
            return null;
        }
    }
}
