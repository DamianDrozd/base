﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;
using BackEnd.Data.Models.Identity;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;

namespace BackEnd.Data.Services
{
    public class JwtService
    {
        private IConfiguration Configuration;

        public JwtService(IConfiguration _configuration)
        {
            Configuration = _configuration;
        }

        public string GenerateSecurityToken(AppUser user, IList<string> roles)
        {
            var claims = new List<Claim>();
            var tokenHandler = new JwtSecurityTokenHandler();
            var jwtKey = Configuration["Environment"].Equals("Development") ? Configuration["Tokens:JwtKey"] : Environment.GetEnvironmentVariable("JwtTokenKey");
            var key = Encoding.ASCII.GetBytes(jwtKey);

            //Populate Claims
            claims.Add(new Claim(ClaimTypes.NameIdentifier, user.Id.ToString()));
            claims.Add(new Claim(ClaimTypes.Email, user.Email));
            claims.Add(new Claim(ClaimTypes.Name, user.UserName));
            foreach (var role in roles)
            {
                claims.Add(new Claim(ClaimTypes.Role, role));
            }

            //Create Token
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(claims),
                Expires = DateTime.UtcNow.AddHours(1),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };

            var token = tokenHandler.CreateToken(tokenDescriptor);

            return tokenHandler.WriteToken(token);
        }

        public void GenerateKey()
        {
            var key = new byte[32];
            using (RandomNumberGenerator rng = new RNGCryptoServiceProvider())
            {
                byte[] tokenData = new byte[32];
                rng.GetBytes(tokenData);

                Configuration["Tokens:Key"] = Convert.ToBase64String(tokenData);
            };
        }
    }
}
