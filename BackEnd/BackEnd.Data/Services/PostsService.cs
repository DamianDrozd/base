﻿using BackEnd.Data.Models.App;
using BackEnd.Data.Models.Identity;
using BackEnd.Core.Models.ViewModels.PostViewModels;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using BackEnd.Core.Utilities;
using BackEnd.Data.Models.Services;

namespace BackEnd.Data.Services
{

    public class PostsService : IPostsService
    {
        private readonly IConfiguration _configuration;
        private readonly AppDbContext _context;
        private UserManager<AppUser> _userManager;

        //Error messages for exception throws
        private const string _errMessageUserNotFound = "Failed to find user.";
        private const string _errMessagePostNotFound = "Post not found.";
        private const string _errMessagePostsNotFound = "Failed to retrieve posts.";
        private const string _errMessageEmailNotConfirmed = "Email not confirmed";

        public PostsService(IConfiguration configuration, AppDbContext context, UserManager<AppUser> userManager)
        {
            _configuration = configuration; 
            _context = context;
            _userManager = userManager;
        }

        //Create a new post
        public async Task Create(PostViewModelCreate _post, string userId)
        {
            var user = await _userManager.FindByIdAsync(userId);
            if (user != null && user.EmailConfirmed)
            {
                var post = new Post()
                {
                    Title = _post.Title,
                    Thumbnail = "",
                    Content = _post.Content,
                    UserId = user.Id,
                    User = user,
                    CreateDateTime = DateTime.Now,
                    UpdateDateTime = DateTime.Now
                };
                _context.Posts.Add(post);
                await _context.SaveChangesAsync();

                //Save the thumbnail to IFS
                try
                {
                    var postThumbnailFileName = post.PostId.ToString() + _post.Thumbnail.FileName;
                    var postThumbnailPath = _configuration["PostThumbnailPathIFS"] + postThumbnailFileName;
                    var fileUtility = new FileUtility();
                    fileUtility.UploadFile(postThumbnailPath, _post.Thumbnail);
                    post.Thumbnail = postThumbnailFileName;
                    _context.Update(post);
                    await _context.SaveChangesAsync();
                }
                catch (Exception exception)
                {
                    _context.Remove(post);
                    await _context.SaveChangesAsync();
                    throw new Exception(exception.Message);
                }
            }
            else
            {
                var errorMessege = user == null ? _errMessageUserNotFound : _errMessageEmailNotConfirmed;
                throw new Exception(errorMessege);
            }
        }
        
        //Get a post specified by id
        public async Task<PostViewModel> GetPostByPostId(int postId)
        {
            var post = await _context.Posts.Include(p => p.User).Where(p => p.PostId == postId).FirstOrDefaultAsync();
            if (post != null)
            {
                var _post = new PostViewModel()
                {
                    PostId = post.PostId,
                    UserName = post.User.UserName,
                    Title = post.Title,
                    Thumbnail = _configuration["PostThumbnailPathWeb"] + post.Thumbnail,
                    Content = post.Content
                };

                return _post;
            }
            throw new Exception(_errMessagePostNotFound);
        }

        //Get All posts
        public async Task<List<PostViewModel>> GetPosts()
        {
            var _posts = new List<PostViewModel>();
            var posts = await _context.Posts.Include(p => p.User).ToListAsync();
            if (posts != null)
            {
                var postThumbnailPathWeb = _configuration["PostThumbnailPathWeb"];
                foreach (var post in posts)
                {
                    _posts.Add(new PostViewModel()
                    {
                        PostId = post.PostId,
                        UserName = post.User.UserName,
                        Title = post.Title,
                        Thumbnail = postThumbnailPathWeb + post.Thumbnail,
                        Content = post.Content

                    });
                }
                return _posts;
            }
            throw new Exception(_errMessagePostsNotFound);
        }

        //Get all posts associated to user
        public async Task<List<PostViewModel>> GetUserPosts(string userName)
        {
            //get all posts related to the user specified
            var _posts = new List<PostViewModel>();
            var posts = await _context.Posts.Include(p => p.User).Where(p => p.User.NormalizedUserName == userName.ToUpper()).ToListAsync();
            if(posts != null && posts.Count > 0)
            {
                //prepare the posts for display by adding them to a view model
                var postThumbnailPathWeb = _configuration["PostThumbnailPathWeb"];
                foreach (var post in posts)
                {
                    _posts.Add(new PostViewModel()
                    {
                        PostId = post.PostId,
                        UserName = post.User.UserName,
                        Title = post.Title,
                        Thumbnail = postThumbnailPathWeb + post.Thumbnail,
                        Content = post.Content
                    });
                }
                return _posts;
            }
            throw new Exception(_errMessagePostsNotFound);
        }

        //Update post details
        public async Task Update(int postId, PostViewModelUpdate _post, string userId)
        {
            //Confirm user and user exists and load info
            var user = await _userManager.FindByIdAsync(userId);
            if (user != null)
            {
                var post = await _context.Posts.Include(p => p.User).Where(p => (p.User.Id == user.Id && p.PostId == postId)).FirstOrDefaultAsync();
                if (post != null)
                {
                    post.Title = post.Title.Equals(_post.Title) ? post.Title : _post.Title;
                    post.Content = post.Content.Equals(_post.Content) ? post.Content : _post.Content;
                    post.UpdateDateTime = DateTime.Now;

                    //Thumbnail will be null if not changed otherwise update the file systen with new image
                    if (_post.Thumbnail != null)
                    {
                        var newPostThumbnailFileName = post.PostId.ToString() + _post.Thumbnail.FileName;
                        var fileUtility = new FileUtility();
                        //Delete current thumbnail
                        fileUtility.DeleteFile(_configuration["PostThumbnailPathIFS"] + post.Thumbnail);
                        //Save the new image
                        fileUtility.UploadFile(_configuration["PostThumbnailPathIFS"] + newPostThumbnailFileName, _post.Thumbnail);
                        post.Thumbnail = newPostThumbnailFileName;
                    }
                    //save post
                    _context.Entry(post).State = EntityState.Modified;
                    await _context.SaveChangesAsync();
                }
                else
                {
                    throw new Exception(_errMessagePostNotFound);
                }
            }
            else
            {
                throw new Exception(_errMessageUserNotFound);
            }
        }

        //Add posts to a users view history when a user opens a post
        public async Task AddPostToUserHistory(string userId, int postId)
        {
            var userHistory = new UserHistory()
            {
                UserId = new Guid(userId),
                PostId = postId,
                ViewDate = DateTime.Now
            };
            _context.UserHistory.Add(userHistory);
            await _context.SaveChangesAsync();
        }

        //Get users post view history
        public async Task<List<PostUserHistory>> GetUserHistory(string userId)
        {

            var _userHistory = new List<PostUserHistory>();

            //Get all users history and prepare for display
            var userHistory = await _context.UserHistory.Include(uh => uh.User).Include(uh => uh.Post).Where(uh => uh.User.Id == new Guid(userId)).ToListAsync();
            if (userHistory != null)
            {
                var postThumbnailPathWeb = _configuration["PostThumbnailPathWeb"];
                foreach(var view in userHistory)
                {
                    _userHistory.Add(new PostUserHistory() { 
                        PostId = view.PostId,
                        Thumbnail = postThumbnailPathWeb + view.Post.Thumbnail,
                        Title = view.Post.Title,
                        UserName = view.User.UserName,
                        ViewDate = view.ViewDate
                    });
                }
            }
            return _userHistory;
        }

    }
}
