﻿using BackEnd.Core.Models.ViewModels.AccountViewModels;
using BackEnd.Data.Models.Identity;
using BackEnd.Data.Services;
using BackEnd.Tests.Helpers;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Configuration;
using Moq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace BackEnd.Tests
{
    public class AccountTests
    {
        private IConfiguration _configuration;
        private IdentityMockHelper _identityMockHelper;

        public AccountTests()
        {
            _identityMockHelper = new IdentityMockHelper();
            _configuration = new ConfigurationHelper().Configuration;
        }

        [Theory]
        [InlineData("tester", "tester@email.com", "P@ssword123", "1231231231", "FirstName", "LastName")]
        public async Task Register_Valid(string userName, string email, string password, string phoneNumber, string firstName, string lastName)
        {
            //Arrange
            var newAccount = new AccountRegisterViewModel()
            {
                UserName = userName,
                Email = email,
                Password = password,
                PhoneNumber = phoneNumber,
                FirstName = firstName,
                LastName = lastName
            };
            var newUser = new AppUser()
            {
                UserName = newAccount.UserName,
                Email = newAccount.Email,
                PhoneNumber = newAccount.PhoneNumber,
                FirstName = newAccount.FirstName,
                LastName = newAccount.LastName,
                TwoFactorEnabled = false
            };

            var userManager = _identityMockHelper.SetupUserManager();
            userManager.Setup(x => x.CreateAsync(It.IsAny<AppUser>(), It.IsAny<string>())).ReturnsAsync(IdentityResult.Success).Verifiable();
            userManager.Setup(x => x.AddToRoleAsync(It.IsAny<AppUser>(), It.IsAny<string>())).ReturnsAsync(IdentityResult.Success).Verifiable();
            userManager.Setup(x => x.GetRolesAsync(It.IsAny<AppUser>())).ReturnsAsync(new List<string>() { "Role1" }).Verifiable();
            userManager.Setup(x => x.CheckPasswordAsync(It.IsAny<AppUser>(), It.IsAny<string>())).ReturnsAsync(true).Verifiable();
            userManager.Setup(m => m.SupportsUserLockout).Returns(false).Verifiable();
            userManager.Setup(m => m.IsLockedOutAsync(newUser)).ReturnsAsync(false).Verifiable();
            userManager.Setup(m => m.DeleteAsync(It.IsAny<AppUser>())).ReturnsAsync(IdentityResult.Success).Verifiable();

            var signInManager = _identityMockHelper.SetupSignInManager(userManager.Object);

            var accountService = new AccountService(_configuration, userManager.Object, signInManager);

            //Act
            var actual = await accountService.Register(newAccount);

            //Assert
            Assert.IsType<AccountTokenModel>(actual);
            Assert.NotNull(actual.Token);
            Assert.NotEmpty(actual.Token);
        }

        [Theory]
        [InlineData(null, "tester@email.com", "P@ssword123", "1231231231", "FirstName", "LastName")]
        public async Task RegisterMissingParam_Fail(string userName, string email, string password, string phoneNumber, string firstName, string lastName)
        {
            //Arrange
            var newAccount = new AccountRegisterViewModel()
            {
                UserName = userName,
                Email = email,
                Password = password,
                PhoneNumber = phoneNumber,
                FirstName = firstName,
                LastName = lastName
            };
            var newUser = new AppUser()
            {
                UserName = newAccount.UserName,
                Email = newAccount.Email,
                PhoneNumber = newAccount.PhoneNumber,
                FirstName = newAccount.FirstName,
                LastName = newAccount.LastName,
                TwoFactorEnabled = false
            };

            var userManager = _identityMockHelper.SetupUserManager();
            userManager.Setup(x => x.CreateAsync(It.IsAny<AppUser>(), It.IsAny<string>())).ReturnsAsync(IdentityResult.Failed()).Verifiable();
            userManager.Setup(x => x.AddToRoleAsync(It.IsAny<AppUser>(), It.IsAny<string>())).ReturnsAsync(IdentityResult.Success).Verifiable();
            userManager.Setup(x => x.GetRolesAsync(It.IsAny<AppUser>())).ReturnsAsync(new List<string>() { "Role1" }).Verifiable();
            userManager.Setup(x => x.CheckPasswordAsync(It.IsAny<AppUser>(), It.IsAny<string>())).ReturnsAsync(true).Verifiable();
            userManager.Setup(m => m.SupportsUserLockout).Returns(false).Verifiable();
            userManager.Setup(m => m.IsLockedOutAsync(newUser)).ReturnsAsync(false).Verifiable();

            var signInManager = _identityMockHelper.SetupSignInManager(userManager.Object);

            var accountService = new AccountService(_configuration, userManager.Object, signInManager);

            //Act and Assert
            await Assert.ThrowsAsync<Exception>(() => accountService.Register(newAccount));
        }

        [Fact]
        public async Task Login_Valid()
        {

            //Arrange
            var account = new AccountLoginViewModel() { UserName = "tester", Password = "P@ssword123" };
            var user = new AppUser()
            {
                UserName = account.UserName,
                Email = "tester@email.com",
                PhoneNumber = "1231231231",
                FirstName = "test",
                LastName = "er",
                TwoFactorEnabled = false
            };

            var userManager = _identityMockHelper.SetupUserManager();
            userManager.Setup(x => x.GetRolesAsync(It.IsAny<AppUser>())).ReturnsAsync(new List<string>() { "Role1" }).Verifiable();
            userManager.Setup(x => x.CheckPasswordAsync(It.IsAny<AppUser>(), It.IsAny<string>())).ReturnsAsync(true).Verifiable();
            userManager.Setup(m => m.SupportsUserLockout).Returns(false).Verifiable();
            userManager.Setup(m => m.IsLockedOutAsync(user)).ReturnsAsync(false).Verifiable();

            var signInManager = _identityMockHelper.SetupSignInManager(userManager.Object);

            var accountService = new AccountService(_configuration, userManager.Object, signInManager);

            //Act
            var actual = await accountService.Login(account);

            //Assert
            Assert.IsType<AccountTokenModel>(actual);
            Assert.NotNull(actual.Token);
            Assert.NotEmpty(actual.Token);
        }

        [Theory]
        [InlineData("tester", "P@ssword12")]
        [InlineData("teste", "P@ssword123")]
        public async Task Login_Fail(string userName, string password)
        {

            //Arrange
            var account = new AccountLoginViewModel() { UserName = userName, Password = password};
            var user = new AppUser()
            {
                UserName = account.UserName,
                Email = "tester@email.com",
                PhoneNumber = "1231231231",
                FirstName = "test",
                LastName = "er",
                TwoFactorEnabled = false
            };

            var userManager = _identityMockHelper.SetupUserManager();
            userManager.Setup(x => x.GetRolesAsync(It.IsAny<AppUser>())).ReturnsAsync(new List<string>() { "Role1" }).Verifiable();
            userManager.Setup(x => x.CheckPasswordAsync(It.IsAny<AppUser>(), It.IsAny<string>())).ReturnsAsync(account.UserName == "tester" && account.Password == "P@ssword123").Verifiable();
            userManager.Setup(m => m.SupportsUserLockout).Returns(false).Verifiable();
            userManager.Setup(m => m.IsLockedOutAsync(user)).ReturnsAsync(false).Verifiable();

            var signInManager = _identityMockHelper.SetupSignInManager(userManager.Object);

            var accountService = new AccountService(_configuration, userManager.Object, signInManager);

            //Act and Assert
            await Assert.ThrowsAsync<Exception>(() => accountService.Login(account));
        }


        [Fact]
        public async Task Details_Valid()
        {
            //Arrange
            var account = new AccountDetailsViewModel()
            {
                UserName = "tester",
                Email = "tester@email.com",
                PhoneNumber = "1231231231",
                FirstName = "test",
                LastName = "er",
                TwoFactorEnabled = false
            };
            var user = new AppUser()
            {
                UserName = account.UserName,
                Email = account.Email,
                PhoneNumber = account.PhoneNumber,
                FirstName = account.FirstName,
                LastName = account.LastName,
                TwoFactorEnabled = account.TwoFactorEnabled,
            };

            var userManager = _identityMockHelper.SetupUserManager();

            var signInManager = _identityMockHelper.SetupSignInManager(userManager.Object);

            var accountService = new AccountService(_configuration, userManager.Object, signInManager);

            //Act
            var actual = await accountService.Details(user.Id.ToString());

            //Assert
            Assert.IsType<AccountDetailsViewModel>(actual);
            Assert.NotNull(actual);
        }
        
        [Fact]
        public async Task Details_Fail()
        {
            //Arrange
            var user = new AppUser()
            {
                UserName = "tester",
                Email = "tester@email.com",
                PhoneNumber = "1231231231",
                FirstName = "test",
                LastName = "er",
                TwoFactorEnabled = false
            };

            var userManager = _identityMockHelper.SetupUserManager();
            userManager.Setup(m => m.FindByIdAsync(It.IsAny<string>())).ReturnsAsync(() => null);

            var signInManager = _identityMockHelper.SetupSignInManager(userManager.Object);

            var accountService = new AccountService(_configuration, userManager.Object, signInManager);

            //Act and Assert
            await Assert.ThrowsAsync<Exception>(() => accountService.Details(user.Id.ToString()));
        }

        [Fact]
        public async Task Update_NoUser_Fail()
        {
            //Arrange
            var account = new AccountDetailsViewModel()
            {
                UserName = "tester",
                Email = "tester@email.com",
                PhoneNumber = "1231231231",
                FirstName = "test",
                LastName = "er",
                TwoFactorEnabled = false
            };
            var user = new AppUser()
            {
                UserName = account.UserName,
                Email = account.Email,
                PhoneNumber = account.PhoneNumber,
                FirstName = account.FirstName,
                LastName = account.LastName,
                TwoFactorEnabled = account.TwoFactorEnabled,
            };

            var userManager = _identityMockHelper.SetupUserManager();
            userManager.Setup(m => m.FindByIdAsync(It.IsAny<string>())).ReturnsAsync(() => null);

            var signInManager = _identityMockHelper.SetupSignInManager(userManager.Object);

            var accountService = new AccountService(_configuration, userManager.Object, signInManager);

            //Act and Assert
            await Assert.ThrowsAsync<Exception>(() => accountService.Update(account, user.Id.ToString()));
        }

        [Fact]
        public async Task Update_Fail()
        {
            //Arrange
            var account = new AccountDetailsViewModel()
            {
                UserName = "tester",
                Email = "tester@email.com",
                PhoneNumber = "1231231231",
                FirstName = "test",
                LastName = "er",
                TwoFactorEnabled = false
            };
            var user = new AppUser()
            {
                UserName = account.UserName,
                Email = account.Email,
                PhoneNumber = account.PhoneNumber,
                FirstName = account.FirstName,
                LastName = account.LastName,
                TwoFactorEnabled = account.TwoFactorEnabled,
            };

            var userManager = _identityMockHelper.SetupUserManager();
            userManager.Setup(m => m.FindByIdAsync(It.IsAny<string>())).ReturnsAsync(user);
            userManager.Setup(m => m.UpdateAsync(It.IsAny<AppUser>())).ReturnsAsync(IdentityResult.Failed());

            var signInManager = _identityMockHelper.SetupSignInManager(userManager.Object);

            var accountService = new AccountService(_configuration, userManager.Object, signInManager);

            //Act and Assert
            await Assert.ThrowsAsync<Exception>(() => accountService.Update(account, user.Id.ToString()));
        }

        [Fact]
        public async Task Delete_NoUser_Fail()
        {
            //Arrange
            var user = new AppUser()
            {
                UserName = "tester",
                Email = "tester@email.com",
                PhoneNumber = "1231231231",
                FirstName = "test",
                LastName = "er",
                TwoFactorEnabled = false
            };

            var userManager = _identityMockHelper.SetupUserManager();
            userManager.Setup(m => m.FindByIdAsync(It.IsAny<string>())).ReturnsAsync(() => null);

            var signInManager = _identityMockHelper.SetupSignInManager(userManager.Object);

            var accountService = new AccountService(_configuration, userManager.Object, signInManager);

            //Act and Assert
            await Assert.ThrowsAsync<Exception>(() => accountService.Delete(user.Id.ToString()));
        }

        [Fact]
        public async Task Delete_Fail()
        {
            //Arrange
            var user = new AppUser()
            {
                UserName = "tester",
                Email = "tester@email.com",
                PhoneNumber = "1231231231",
                FirstName = "test",
                LastName = "er",
                TwoFactorEnabled = false
            };

            var userManager = _identityMockHelper.SetupUserManager();
            userManager.Setup(m => m.FindByIdAsync(It.IsAny<string>())).ReturnsAsync(user);
            userManager.Setup(m => m.DeleteAsync(It.IsAny<AppUser>())).ReturnsAsync(IdentityResult.Failed());

            var signInManager = _identityMockHelper.SetupSignInManager(userManager.Object);

            var accountService = new AccountService(_configuration, userManager.Object, signInManager);

            //Act and Assert
            await Assert.ThrowsAsync<Exception>(() => accountService.Delete(user.Id.ToString()));
        }
    }
}
