﻿using BackEnd.Data;
using BackEnd.Data.Models.App;
using BackEnd.Data.Models.Identity;
using Microsoft.EntityFrameworkCore;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;

namespace BackEnd.Tests.Helpers
{
    class DataMockHelpers
    {
        public readonly Post DefaultPost = new Post()
        {
            PostId = 1,
            Title = "test title",
            Content = "test content",
            CreateDateTime = new DateTime(),
            UpdateDateTime = new DateTime(),
            UserId = new Guid(),
            User = new AppUser()
        };

        public AppDbContext SetupAppDbContext()
        {
            var builder = new DbContextOptionsBuilder<AppDbContext>();
            builder.UseInMemoryDatabase(databaseName: "BaseTestDatabase");
            var options = builder.Options; 

            var appDbContext = new AppDbContext(options);

            //Add 1 post in the database to test with
            if(!appDbContext.Posts.Any())
            {
                var identityMockHelper = new IdentityMockHelper();
                var user = identityMockHelper.DefaultAppUser;
                this.DefaultPost.User = user;
                appDbContext.Posts.Add(this.DefaultPost);
                appDbContext.SaveChanges();
            }
            return appDbContext;
        }
    }
}
