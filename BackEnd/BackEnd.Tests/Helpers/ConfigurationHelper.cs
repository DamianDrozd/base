﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace BackEnd.Tests.Helpers
{
    class ConfigurationHelper
    {
        public IConfiguration Configuration { get; set; }

        public ConfigurationHelper()
        {
            this.Configuration = new ConfigurationBuilder().AddJsonFile(Directory.GetCurrentDirectory() + "/appsettings.json").Build();
        }
    }
}
