﻿using BackEnd.Data.Models.Identity;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging.Abstractions;
using Microsoft.Extensions.Options;
using Moq;
using System.Collections.Generic;

namespace BackEnd.Tests.Helpers
{
    public class IdentityMockHelper
    {
        public readonly AppUser DefaultAppUser = new AppUser()
        {
                UserName = "tester",
                Email = "tester@email.com",
                PhoneNumber = "1231231231",
                FirstName = "test",
                LastName = "er",
                TwoFactorEnabled = false,
                NormalizedUserName = "TESTER",
                NormalizedEmail = "TESTER@EMAIL.COM"
            };

        public Mock<UserManager<AppUser>> SetupUserManager()
        {
            var store = new Mock<IUserStore<AppUser>>();
            var mockUserManager = new Mock<UserManager<AppUser>>(store.Object, null, null, null, null, null, null, null, null);
            mockUserManager.Object.UserValidators.Add(new UserValidator<AppUser>());
            mockUserManager.Object.PasswordValidators.Add(new PasswordValidator<AppUser>());
            mockUserManager.Setup(m => m.FindByNameAsync(DefaultAppUser.UserName)).ReturnsAsync(DefaultAppUser);
            mockUserManager.Setup(m => m.FindByIdAsync(It.IsAny<string>())).ReturnsAsync(DefaultAppUser);
            mockUserManager.Setup(m => m.GetUserIdAsync(DefaultAppUser)).ReturnsAsync(DefaultAppUser.Id.ToString());
            mockUserManager.Setup(m => m.GetUserNameAsync(DefaultAppUser)).ReturnsAsync(DefaultAppUser.UserName);
            mockUserManager.Setup(m => m.DeleteAsync(DefaultAppUser)).ReturnsAsync(IdentityResult.Success);
            return mockUserManager;
        }

        public SignInManager<AppUser> SetupSignInManager(UserManager<AppUser> userManager)
        {
            //Setup sign in
            var context = new DefaultHttpContext();
            var auth = new Mock<IAuthenticationService>();
            context.RequestServices = new ServiceCollection().AddSingleton(auth.Object).BuildServiceProvider();

            //SetupSignInManager SignInManager
            var logger = NullLogger<SignInManager<AppUser>>.Instance;
            var contextAccessor = new Mock<IHttpContextAccessor>();
            contextAccessor.Setup(a => a.HttpContext).Returns(context);
            var roleManager = MockRoleManager<AppRole>();
            var identityOptions = new IdentityOptions();
            var options = new Mock<IOptions<IdentityOptions>>();
            options.Setup(a => a.Value).Returns(identityOptions);
            var claimsFactory = new UserClaimsPrincipalFactory<AppUser, AppRole>(userManager, roleManager.Object, options.Object);
            var schemeProvider = new Mock<IAuthenticationSchemeProvider>().Object;
            var signInManager = new SignInManager<AppUser>(userManager, contextAccessor.Object, claimsFactory, options.Object, logger, schemeProvider, new DefaultUserConfirmation<AppUser>());
            return signInManager;
        }

        private Mock<RoleManager<AppRole>> MockRoleManager<AppRole>(IRoleStore<AppRole> store = null) where AppRole : class
        {
            store = store ?? new Mock<IRoleStore<AppRole>>().Object;
            var roles = new List<IRoleValidator<AppRole>>();
            roles.Add(new RoleValidator<AppRole>());
            return new Mock<RoleManager<AppRole>>(store, roles, new UpperInvariantLookupNormalizer(),
                new IdentityErrorDescriber(), null);
        }

    }
}
