﻿using BackEnd.Core.Models.ViewModels.PostViewModels;
using BackEnd.Data;
using BackEnd.Data.Services;
using BackEnd.Tests.Helpers;
using Microsoft.Extensions.Configuration;
using Moq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace BackEnd.Tests
{
    public class PostsTest
    {
        private IdentityMockHelper _identityMockHelper;
        private DataMockHelpers _dataMockHelper;
        private AppDbContext _appDbContext;
        private IConfiguration _configuration;

        public PostsTest()
        {
            _identityMockHelper = new IdentityMockHelper();
            _dataMockHelper = new DataMockHelpers();
            _appDbContext = _dataMockHelper.SetupAppDbContext();

            _configuration = new ConfigurationHelper().Configuration;
        }

        [Fact]
        public async Task Create_Failed()
        {
            //Arrange
            var newPost = new PostViewModelCreate()
            {
                Title = "test title",
                Content = "test content",
            };

            var userManager = _identityMockHelper.SetupUserManager();
            userManager.Setup(m => m.FindByIdAsync(It.IsAny<string>())).ReturnsAsync(() => null);

            var postsService = new PostsService(_configuration, _appDbContext, userManager.Object);

            //Act and Assert
            await Assert.ThrowsAsync<Exception>(() => postsService.Create(newPost, _identityMockHelper.DefaultAppUser.Id.ToString()));
        }

        [Fact]
        public async Task GetPostById_Valid()
        {
            //Arrange
            var userManager = _identityMockHelper.SetupUserManager();

            var postsService = new PostsService(_configuration, _appDbContext, userManager.Object);

            //Act
            var actual = await postsService.GetPostByPostId(_dataMockHelper.DefaultPost.PostId);

            //Assert
            Assert.IsType<PostViewModel>(actual);
            Assert.NotNull(actual);
        }
        
        [Fact]
        public async Task GetPostById_Fail()
        {
            //Arrange
            var userManager = _identityMockHelper.SetupUserManager();

            var postsService = new PostsService(_configuration, _appDbContext, userManager.Object);

            //Act and Assert
            await Assert.ThrowsAsync<Exception>(() => postsService.GetPostByPostId(-1));
        }

        [Fact]
        public async Task GetPosts_Valid()
        {
            //Arrange
            var userManager = _identityMockHelper.SetupUserManager();

            var postsService = new PostsService(_configuration , _appDbContext, userManager.Object);

            //Act
            var actual = await postsService.GetPosts();

            //Assert
            Assert.IsType<List<PostViewModel>>(actual);
            Assert.NotNull(actual);
            Assert.True(actual.Count == 1);
        }

        [Fact]
        public async Task GetUserPosts_Valid()
        {
            //Arrange
            var userManager = _identityMockHelper.SetupUserManager();

            var postsService = new PostsService(_configuration, _appDbContext, userManager.Object);

            //Act
            var actual = await postsService.GetUserPosts(_identityMockHelper.DefaultAppUser.UserName);

            //Assert
            Assert.IsType<List<PostViewModel>>(actual);
            Assert.NotNull(actual);
            Assert.True(actual.Count == 1);
        }

        [Fact]
        public async Task GetUserPosts_Fail()
        {
            //Arrange
            var userManager = _identityMockHelper.SetupUserManager();

            var postsService = new PostsService(_configuration, _appDbContext, userManager.Object);

            //Act and Assert
            await Assert.ThrowsAsync<Exception>(() => postsService.GetUserPosts("testerx"));
        }

        [Fact]
        public async Task Update_NoUser_Fail()
        {
            //Arrange
            var post = new PostViewModelUpdate() 
            { 
                PostId = _dataMockHelper.DefaultPost.PostId,
                Title = _dataMockHelper.DefaultPost.Title,
                Content = _dataMockHelper.DefaultPost.Content,
                UserName = _identityMockHelper.DefaultAppUser.UserName
            };
            var userManager = _identityMockHelper.SetupUserManager();

            var postsService = new PostsService(_configuration, _appDbContext, userManager.Object);

            //Act and Assert
            await Assert.ThrowsAsync<Exception>(() => postsService.Update(_dataMockHelper.DefaultPost.PostId, post, "1"));
        }

        [Fact]
        public async Task Update_NoPost_Fail()
        {
            //Arrange
            var post = new PostViewModelUpdate() 
            { 
                PostId = _dataMockHelper.DefaultPost.PostId,
                Title = _dataMockHelper.DefaultPost.Title,
                Content = _dataMockHelper.DefaultPost.Content,
                UserName = _identityMockHelper.DefaultAppUser.UserName
            };

            var userManager = _identityMockHelper.SetupUserManager();

            var postsService = new PostsService(_configuration, _appDbContext, userManager.Object);

            //Act and Assert
            await Assert.ThrowsAsync<Exception>(() => postsService.Update(2, post, _identityMockHelper.DefaultAppUser.Id.ToString()));
        }
    }
}
