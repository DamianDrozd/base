﻿using SendGrid;
using SendGrid.Helpers.Mail;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Runtime.InteropServices.ComTypes;
using System.Text;
using System.Threading.Tasks;

namespace BackEnd.Core.Utilities
{
    public class EmailUtility
    {
        private SendGridClient _client;

        private const string _errMessageCreateEmailFailed = "Failed to send email. Reason: ";

        public EmailUtility(string apiKey)
        {
            _client = new SendGridClient(apiKey);
        }

        public async Task SendEmailByTemplate(string fromEmail, string fromName, string toEmail, string toName, string templateId, object templateData)
        {
            try
            {
                var from = new EmailAddress(fromEmail, fromName);
                var to = new EmailAddress(toEmail, toName);
                var msg = MailHelper.CreateSingleTemplateEmail(from, to, templateId, templateData);

                var response = await _client.SendEmailAsync(msg);
                if (!new HttpResponseMessage(response.StatusCode).IsSuccessStatusCode)
                {
                    throw new Exception("Email Api Error. Status:" + response.StatusCode + " response:" + response.ToString());
                }
            }
            catch (Exception exception)
            {
                throw new Exception(_errMessageCreateEmailFailed + exception.Message);
            }
        }
    }
}
