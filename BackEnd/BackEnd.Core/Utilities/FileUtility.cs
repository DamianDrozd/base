﻿using BackEnd.Core.Models.FileModels;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace BackEnd.Core.Utilities
{
    public class FileUtility
    {
        public void UploadFile(string path, IFormFile file)
        {
            try
            {
                using (FileStream fileStream = File.Create(path))
                {
                    file.CopyTo(fileStream);
                    fileStream.Flush();
                }
            }
            catch (Exception exception)
            {
                throw new Exception("Failed to create file. Reason:" + exception.Message);
            }
        }

        public void DeleteFile(string path)
        {
            try
            {
                if(File.Exists(path))
                {
                    File.Delete(path);
                }
            }
            catch (Exception exception)
            {
                throw new Exception("Failed to delete file. Reason:" + exception.Message);
            }
        }
    }
}
