﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Text;

namespace BackEnd.Core.Models.FileModels
{
    public class FormFile
    {
        public IFormFile files { get; set; }
    }
}
