﻿using BackEnd.Core.Models.FileModels;
using Microsoft.AspNetCore.Http;
using System;
using System.ComponentModel.DataAnnotations;

namespace BackEnd.Core.Models.ViewModels.PostViewModels
{
    public class PostViewModel
    {
        public int PostId { get; set; }
        public string UserName { get; set; }
        [Required]
        public string Title { get; set; }
        public string Thumbnail { get; set; }
        [Required]
        public string Content { get; set; }
    }

    public class PostViewModelCreate
    {
        [Required]
        public string Title { get; set; }
        public IFormFile Thumbnail { get; set; }
        [Required]
        public string Content { get; set; }
    }

    public class PostViewModelUpdate
    {
        [Required]
        public int PostId { get; set; }
        public string UserName { get; set; }
        [Required]
        public string Title { get; set; }
        public IFormFile Thumbnail { get; set; }
        [Required]
        public string Content { get; set; }
    }

    public class PostUserHistory
    {
        public int PostId { get; set; }
        public string UserName { get; set; }
        public string Title { get; set; }
        public string Thumbnail { get; set; }
        public DateTime ViewDate { get; set; }
    }

}
