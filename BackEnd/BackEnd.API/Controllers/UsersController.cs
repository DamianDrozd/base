﻿using System.Threading.Tasks;
using BackEnd.Data.Models.Identity;
using BackEnd.Core.Models.ViewModels.UserViewModels;
using BackEnd.Data.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System;
using BackEnd.Data.Models.Services;

namespace back_end.Controllers
{

    [Route("[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class UsersController : Controller
    {
        private IUsersService _usersService;

        public UsersController(UserManager<AppUser> userManager, RoleManager<AppRole> roleManager)
        {
            _usersService = new UsersService(userManager, roleManager);
        }

        [HttpPost]
        [Route("create")]
        public async Task<IActionResult> Create(UserCreateViewModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    await _usersService.Create(model);
                    return Ok();
                }
                catch (Exception exception)
                {
                    ModelState.AddModelError(string.Empty, exception.Message);
                }
            }
            return BadRequest(ModelState);
        }

        [HttpGet]
        [Route("get/{userName}")]
        public async Task<IActionResult> GetUserByUserName(string userName)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    var _user = await _usersService.GetByUserName(userName);
                    return Ok(_user);
                }
                catch (Exception exception)
                {
                    ModelState.AddModelError(string.Empty, exception.Message);
                }
            }
            return BadRequest(ModelState);
        }

        [HttpGet]
        [Route("search/{searchText}")]
        public async Task<IActionResult> SearchUsers(string searchText)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    var _users = await _usersService.SearchUsers(searchText);
                    return Ok(_users);
                }
                catch (Exception exception)
                {
                    ModelState.AddModelError(string.Empty, exception.Message);
                }
            }
            return BadRequest(ModelState);
        }

        [HttpPut]
        [Route("update")]
        public async Task<IActionResult> Update(UserViewModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    await _usersService.Update(model);
                    return Ok();
                }
                catch (Exception exception)
                {
                    ModelState.AddModelError(string.Empty, exception.Message);
                }
            }
            return BadRequest(ModelState);
        }

        [HttpGet]
        [Route("roles")]
        public IActionResult GetUserRoles()
        {
            try
            {
                var _roles = _usersService.GetUserRoles();
                return Ok(_roles);
            }
            catch (Exception exception)
            {
                ModelState.AddModelError(string.Empty, exception.Message);
            }
            return BadRequest();
        }
    }
}