﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using BackEnd.Data.Models.App;
using BackEnd.Data.Models.Identity;
using Microsoft.AspNetCore.Identity;
using System.Security.Claims;
using Microsoft.AspNetCore.Authorization;
using BackEnd.Core.Models.ViewModels.PostViewModels;
using BackEnd.Data.Services;
using BackEnd.Data;
using System;
using Microsoft.Extensions.Configuration;
using BackEnd.Data.Models.Services;

namespace back_end.Controllers
{
    [Route("[controller]")]
    [ApiController]
    [Authorize]
    public class PostsController : ControllerBase
    {
        private IPostsService _postsService;
        private readonly string _errorMessageTitle = "Message";

        public PostsController(IConfiguration configuration, AppDbContext context, UserManager<AppUser> userManager)
        {
            _postsService = new PostsService(configuration, context, userManager);
        }

        // POST: /posts/create
        // Create a new post
        [HttpPost]
        [Route("create")]
        public async Task<ActionResult<Post>> CreatePost([FromForm] PostViewModelCreate _post)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    await _postsService.Create(_post, User.FindFirstValue(ClaimTypes.NameIdentifier));
                    return Ok();
                }
                catch (Exception exception)
                {
                    ModelState.AddModelError(_errorMessageTitle, exception.Message);
                }
            }
            return BadRequest(ModelState);
        }

        // GET: /posts/get/{postId}
        // Fetch post by PostId
        [HttpGet]
        [Route("get/id/{postId}")]
        [AllowAnonymous]
        public async Task<ActionResult<PostViewModel>> GetPost(int postId)
        {
            try
            {
                var userId = User.FindFirstValue(ClaimTypes.NameIdentifier);
                var _post = await _postsService.GetPostByPostId(postId);
                if (userId != null)
                {
                    await _postsService.AddPostToUserHistory(userId, _post.PostId);
                }
                return Ok(_post);
            }
            catch (Exception exception)
            {
                ModelState.AddModelError(_errorMessageTitle, exception.Message);
            }
            return BadRequest();
        }

        // GET: posts/get
        // Get all posts
        [HttpGet]
        [Route("get")]
        [AllowAnonymous]
        public async Task<ActionResult<IEnumerable<PostViewModel>>> GetPosts()
        {
            try
            {
                var _posts = await _postsService.GetPosts();
                return Ok(_posts);
            }
            catch (Exception exception)
            {
                ModelState.AddModelError(_errorMessageTitle, exception.Message);
            }
            return BadRequest(ModelState);
        }

        // GET: posts/get/{userName}
        // Get posts by UserName
        [HttpGet]
        [Route("get/username/{userName}")]
        [AllowAnonymous]
        public async Task<ActionResult<IEnumerable<PostViewModel>>> GetPostsUser(string userName)
        {
            try
            {
                var _posts = await _postsService.GetUserPosts(userName);
                return Ok(_posts);
            }
            catch (Exception exception)
            {
                ModelState.AddModelError(_errorMessageTitle, exception.Message);
            }
            return BadRequest(ModelState);
        }

        // PUT: posts/put/{postId}
        // Update an existing post
        [HttpPut]
        [Route("update/{postId}")]
        public async Task<IActionResult> UpdatePost(int postId, [FromForm]PostViewModelUpdate _post)
        {
            try
            {
                await _postsService.Update(postId, _post, User.FindFirstValue(ClaimTypes.NameIdentifier));
                return Ok();
            }
            catch (Exception exception)
            {
                ModelState.AddModelError(_errorMessageTitle, exception.Message);
            }
            return BadRequest(ModelState);
        }

        [HttpGet]
        [Route("user/history")]
        public async Task<IActionResult> GetUserHistory()
        {
            try
            {
                var userHistory = await _postsService.GetUserHistory(User.FindFirstValue(ClaimTypes.NameIdentifier));
                return Ok(userHistory);
            }
            catch (Exception exception)
            {
                ModelState.AddModelError("errors", exception.Message);
            }
            return BadRequest(ModelState);
        }

        //// DELETE: api/Posts/5
        //[HttpDelete("{id}")]
        //public async Task<ActionResult<Post>> DeletePost(int id)
        //{
        //    var post = await _context.Posts.FindAsync(id);
        //    if (post == null)
        //    {
        //        return NotFound();
        //    }

        //    _context.Posts.Remove(post);
        //    await _context.SaveChangesAsync();

        //    return post;
        //}

    }
}
