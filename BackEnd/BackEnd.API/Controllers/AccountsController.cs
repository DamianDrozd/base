﻿using System.Security.Claims;
using System.Threading.Tasks;
using BackEnd.Data.Models.Identity;
using BackEnd.Data.Services;
using BackEnd.Core.Models.ViewModels.AccountViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using BackEnd.Data.Models.Services;

namespace BackEnd.API.Controllers
{
    [Route("account")]
    [ApiController]
    [Authorize]
    public class AccountsController : ControllerBase
    {

        private IAccountService _accountService;

        public AccountsController(IConfiguration configuration, UserManager<AppUser> userManager, SignInManager<AppUser> signInManager)
        {
            _accountService = new AccountService(configuration, userManager, signInManager);
        }
        
        [HttpPost]
        [AllowAnonymous]
        [Route("register")]
        public async Task<IActionResult> Register(AccountRegisterViewModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    var tokenModel = await _accountService.Register(model);
                    return Ok(tokenModel);
                }
                catch (Exception exception)
                {
                    ModelState.AddModelError("errors", exception.Message);
                }
            }
            return BadRequest(ModelState);
        }

        [HttpPost]
        [AllowAnonymous]
        [Route("register-external")]
        public async Task<IActionResult> RegisterExternal(AccountExternalModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    var tokenModel = await _accountService.RegisterExternal(model);
                    return Ok(tokenModel);
                }
                catch (Exception exception)
                {

                    ModelState.AddModelError("errors", exception.Message); ;
                }
            }
            return BadRequest(ModelState);
        }

        [HttpPost]
        [AllowAnonymous]
        [Route("password/reset/send")]
        public async Task<IActionResult> SendPasswordReset(AccountSendPasswordResetModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    await _accountService.SendPasswordReset(model.Id);
                    return Ok();
                }
                catch (Exception exception)
                {
                    ModelState.AddModelError("errors", exception.Message);
                }
            }
            return BadRequest(ModelState);
        }

        [HttpPost]
        [AllowAnonymous]
        [Route("password/reset")]
        public async Task<IActionResult> PasswordReset(AccountPasswordResetModel model)
        {
            try
            {
                await _accountService.PasswordReset(model.UserName, model.Token, model.Password);
                return Ok();
            }
            catch (Exception exception)
            {

                ModelState.AddModelError("errors", exception.Message);
            }
            return BadRequest(ModelState);
        }

        [HttpPost]
        [Route("email/confirm")]
        public async Task<IActionResult> EmailConfirm(AccountEmailConfirmModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    await _accountService.EmailConfirm(User.FindFirstValue(ClaimTypes.NameIdentifier), model.Token);
                    return Ok();
                }
                catch (Exception exception)
                {
                    ModelState.AddModelError("errors", exception.Message);
                }
            }
            return BadRequest(ModelState);
        }

        [HttpGet]
        [Route("email/confirm/send")]
        public async Task<IActionResult> EmailConfirmSend()
        {
            try
            {
                await _accountService.EmailConfirmSend(User.FindFirstValue(ClaimTypes.NameIdentifier));
                return Ok();
            }
            catch (Exception exception)
            {

                ModelState.AddModelError("errors", exception.Message);
            }
            return BadRequest();
        }

        [HttpPost]
        [AllowAnonymous]
        [Route("login")]
        public async Task<IActionResult> Login(AccountLoginViewModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    var tokenModel = await _accountService.Login(model);
                    return Ok(tokenModel);
                }
                catch (Exception exception)
                {
                    ModelState.AddModelError("errors", exception.Message);
                }
            }
            return BadRequest(ModelState);
        }

        [HttpPost]
        [AllowAnonymous]
        [Route("login-external")]
        public async Task<IActionResult> LoginExternal(AccountExternalModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    var tokenModel = await _accountService.LoginExternal(model);
                    return Ok(tokenModel);
                }catch(Exception exception)
                {
                    ModelState.AddModelError("errors", exception.Message);
                }
            }
            return BadRequest(ModelState);
        }

        [HttpGet]
        [Route("details")]
        public async Task<IActionResult> Details()
        {
            try
            {
                var _account = await _accountService.Details(User.FindFirstValue(ClaimTypes.NameIdentifier));
                return Ok(_account);
            }
            catch (Exception exception)
            {
                ModelState.AddModelError("errors", exception.Message);
            }

            return BadRequest();
        }

        [HttpPost]
        [Route("update")]
        public async Task<IActionResult> Update(AccountDetailsViewModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    await _accountService.Update(model, User.FindFirstValue(ClaimTypes.NameIdentifier));
                    return Ok();
                }
                catch (Exception exception)
                {
                    ModelState.AddModelError("errors", exception.Message);
                }
            }
            return BadRequest(ModelState);
        }

        [HttpDelete]
        [Route("delete")]
        public async Task<IActionResult> Delete()
        {
            try
            {
                await _accountService.Delete(User.FindFirstValue(ClaimTypes.NameIdentifier));
                return Ok();
            }
            catch (Exception exception)
            {
                ModelState.AddModelError("errors", exception.Message);
            }
            return BadRequest(ModelState);
        }
    }
}